# SubspecsTest

[![CI Status](http://img.shields.io/travis/huluobo/SubspecsTest.svg?style=flat)](https://travis-ci.org/huluobo/SubspecsTest)
[![Version](https://img.shields.io/cocoapods/v/SubspecsTest.svg?style=flat)](http://cocoapods.org/pods/SubspecsTest)
[![License](https://img.shields.io/cocoapods/l/SubspecsTest.svg?style=flat)](http://cocoapods.org/pods/SubspecsTest)
[![Platform](https://img.shields.io/cocoapods/p/SubspecsTest.svg?style=flat)](http://cocoapods.org/pods/SubspecsTest)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SubspecsTest is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'SubspecsTest'
```

## Author

huluobo, hujewelz@163.com

## License

SubspecsTest is available under the MIT license. See the LICENSE file for more info.
